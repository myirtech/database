#!/usr/bin/env python3


# name: name of column
# type: data type of column
# default: value to use if column is not known
# relation: name of file to create relation into for this field (and to avoid messy datasets)
# label: short description

fields = [
    {
        'name': 'name',
        'type': 'string',
        'default': '',
        'label': 'Name of the board (e.g. Raspberry Pi 3 Model B)'
    },
    {
        'name': 'manufacturer',
        'type': 'reference',
        'relation': 'manufacturer.yaml',
        'default': None,
        'label': 'Manufacturer'
    },
    {
        'name': '_deprecated',
        'type': 'boolean',
        'default': False,
        'label': 'Board is discontinued or no longer supported by manufacturer'
    },
    {
        'name': '_description',
        'type': 'string',
        'default': None,
        'label': 'Long description of the board (ideally 3-5 sentences)'
    },
    {
        'name': '_links',
        'type': 'json',
        'default': [],
        'label': 'Links - TODO'
    },
    {
        'name': '_notes',
        'type': 'string',
        'default': '',
        'label': 'Internal notes - for maintainers and future contributors, e.g. about some bus or connector which is not yet supported by the schema'
    },
    {
        'name': '_slug',
        'type': 'string',
        'default': '',
        'label': 'Board identifier for URL, in the form of {manufacturer}-{name}. Letters should be lowercase, numbers and dashes should be used as separators. Look at the existing boards for examples.'
    },
    {
        'name': '_price',
        'type': 'integer',
        'unit': 'USD',
        'default': 0,
        'label': 'Board official (retail / catalog) price'
    },
    {
        'name': '_url',
        'type': 'string',
        'default': '',
        'label': 'Board official URL. Permalink, please link to the official product page and remove any tracking parameters'
    },
    {
        'name': '_visibility',
        'type': 'string',
        'default': 1,
        'label': 'Visibility of the board. 0 = hidden, 1 = visible'
    },
    {
        'name': 'architecture', 
        'type': 'reference', 
        'relation': 'architecture.yaml', 
        'default': None,
        'label': 'CPU architecture'
    },
    {
        'name': 'audio_i2s', 
        'type': 'integer', 
        'unit': 'count',
        'default': 0, 
        'label': 'I2S audio channels'
    },
    {
        'name': 'audio_line_in',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Line-in audio channels'
    },
    {
        'name': 'audio_mic_in',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Microphone audio channels'
    },
    {
        'name': 'audio_mic_onboard',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Onboard microphone audio channels'
    },
    {
        'name': 'audio_outputs',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Audio analog output channels'
    },
    {
        'name': 'audio_spdif_in',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'SPDIF audio input channels'
    },
    {
        'name': 'audio_spdif_out',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'SPDIF audio output channels'
    },
    {
        'name': 'camera_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Camera inputs (all types)'
    },
    {
        'name': 'camera_mipi',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Camera inputs (MIPI CSI)'
    },
    {
        'name': 'camera_parallel',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Camera inputs (parallel)'
    },
    {
        'name': 'certs',
        'type': 'json',
        'default': None,
        'label': 'Certifications'
    },
    {
        'name': 'cpu_board_mcu',
        'type': 'reference',
        'relation': 'mcu.yaml',
        'default': None,
        'label': 'On-board MCU (Microcontroller Unit) model'
    },
    {
        'name': 'cpu_board_mcu_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'On-board MCU (Microcontroller Unit)'
    },
    {
        'name': 'cpu_cluster1_core',
        'type': 'reference',
        'relation': 'core.yaml',
        'default': None,
        'label': 'CPU - Primary cluster - core type'
    },
    {
        'name': 'cpu_cluster1_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'CPU - Primary cluster - core count'
    },
    {
        'name': 'cpu_cluster1_speed',
        'type': 'integer',
        'unit': 'MHz',
        'default': 0,
        'label': 'CPU - Primary cluster - core speed'
    },
    {
        'name': 'cpu_cluster2_core',
        'type': 'reference',
        'relation': 'core.yaml',
        'default': None,
        'label': 'CPU - Secondary cluster - core type'
    },
    {
        'name': 'cpu_cluster2_count',
        'type': 'integer',
        'unit': 'count',
        'default': None,
        'label': 'CPU - Secondary cluster - core count'
    },
    {
        'name': 'cpu_cluster2_speed',
        'type': 'integer',
        'unit': 'MHz',
        'default': None,
        'label': 'CPU - Secondary cluster - core speed'
    },
    {
        'name': 'cpu_cluster3_core',
        'type': 'reference',
        'relation': 'core.yaml',
        'default': None,
        'label': 'CPU - Tertiary cluster - core type'
    },
    {
        'name': 'cpu_cluster3_count',
        'type': 'integer',
        'unit': 'count',
        'default': None,
        'label': 'CPU - Tertiary cluster - core count'
    },
    {
        'name': 'cpu_cluster3_speed',
        'type': 'integer',
        'unit': 'MHz',
        'default': None,

        'label': 'CPU - Tertiary cluster - core speed'
    },
    {
        'name': 'cpu_embedded_mcu',
        'type': 'reference',
        'relation': 'mcu.yaml',
        'default': None,
        'label': 'Embedded MCU (inside the SoC) - model'
    },
    {
        'name': 'cpu_embedded_mcu_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Embedded MCUs (inside the SoC)'
    },
    {
        'name': 'cpu_fpga',
        'type': 'reference',
        'relation': 'fpga.yaml',
        'default': None,
        'label': 'FPGA - model'
    },
    {
        'name': 'cpu_fpga_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'FPGAs'
    },
    {
        'name': 'cpu_npu',
        'type': 'reference',
        'relation': 'npu.yaml',
        'default': None,
        'label': 'Neural Processing Unit (NPU)'
    },
    {
        'name': 'cpu_npu_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Neural Processing Units (NPUs)'
    },
    {
        'name': 'cpu_soc',
        'type': 'reference',
        'relation': 'soc.yaml',
        'default': None,
        'label': 'System on Chip (SoC)'
    },
    {
        'name': 'ethernet_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Ethernet ports'
    },
    {
        'name': 'ethernet_100',
        'type': 'boolean',
        'default': False,
        'label': 'Ethernet ports - 100 Mbps support'
    },
    {
        'name': 'ethernet_1000',
        'type': 'boolean',
        'default': False,
        'label': 'Ethernet ports - 1000 Mbps / Gigabit support'
    },
    {
        'name': 'ethernet_10000',
        'type': 'boolean',
        'default': False,
        'label': 'Ethernet ports - 10000 Mbps / 10 Gigabit support'
    },
    {
        'name': 'ethernet_100000',
        'type': 'boolean',
        'default': False,
        'label': 'Ethernet ports - 100000 Mbps / 100 Gigabit support'
    },
    {
        'name': 'ethernet_2500',
        'type': 'boolean',
        'default': False,
        'label': 'Ethernet ports - 2500 Mbps / 2.5 Gigabit support'
    },
    {
        'name': 'ethernet_5000',
        'type': 'boolean',
        'default': False,
        'label': 'Ethernet ports - 5000 Mbps / 5 Gigabit support'
    },
    {
        'name': 'ethernet_4000',
        'type': 'boolean',
        'default': False,
        'label': 'Ethernet ports - 4000 Mbps / 4 Gigabit support'
    },
    {
        'name': 'ethernet_400000',
        'type': 'boolean',
        'default': False,
        'label': 'Ethernet ports - 400000 Mbps / 400 Gigabit support'
    },
    {
        'name': 'ethernet_poe_active',
        'type': 'boolean',
        'default': False,
        'label': 'Ethernet ports - PoE (Power over Ethernet) - active'
    },
    {
        'name': 'ethernet_poe_passive',
        'type': 'boolean',
        'default': False,
        'label': 'Ethernet ports - PoE (Power over Ethernet) - passive'
    },
    {
        'name': 'ethernet_sfp',
        'type': 'boolean',
        'default': False,
        'label': 'Ethernet ports - SFP (Small Form-factor Pluggable) support'
    },
    {
        'name': 'ethernet_slots',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Ethernet slots - TODO'
    },
    {
        'name': 'ethernet_wol',
        'type': 'boolean',
        'default': False,
        'label': 'Ethernet ports - Wake-on-LAN (WOL) support'
    },
    {
        'name': 'formfactor',
        'type': 'reference',
        'relation': 'formfactor.yaml',
        'default': None,
        'label': 'Form factor'
    },
    {
        'name': 'gpio_adc_count', 
        'type': 'integer', 
        'unit': 'count',
        'default': 0, 
        'label': 'ADC (Analog to Digital Converters) pins'
    },
    {
        'name': 'gpio_can_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'CAN bus channels'
    },
    {
        'name': 'gpio_compatible_hat',
        'type': 'boolean',
        'default': False,
        'label': 'Pin header - compatibility with Raspberry Pi HAT'
    },
    {
        'name': 'gpio_compatible_shield',
        'type': 'boolean',
        'default': False,
        'label': 'Pin header - compatibility with Arduino shield'
    },
    {
        'name': 'gpio_dac_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Analog-to-digital converters'
    },
    {
        'name': 'gpio_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'General-purpose input/output (GPIO) pins'
    },
    {
        'name': 'gpio_i2c_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'I/O - Inter-Integrated Circuit (I2C) interfaces'
    },
    {
        'name': 'gpio_pwm_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'I/O - PWM (Pulse Width Modulation) interfaces'
    },
    {
        'name': 'gpio_spi_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'SPI interfaces (not counting the ones used for flash)'
    },
    {
        'name': 'gpio_uart_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'UART interfaces'
    },
    {
        'name': 'gpu',
        'type': 'reference',
        'relation': 'gpu.yaml',
        'default': None,
        'label': 'GPU - model'
    },
    {
        'name': 'gpu_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'GPUs'
    },
    {
        'name': 'gpu_speed',
        'type': 'integer',
        'unit': 'MHz',
        'default': 0,
        'label': 'GPU - speed'
    },
    {
        'name': 'ir_receiver',
        'type': 'boolean',
        'default': False,
        'label': 'Infrared (IR) receiver'
    },
    {
        'name': 'ir_transmitter',
        'type': 'boolean',
        'default': False,
        'label': 'Infrared (IR) transmitter'
    },
    {
        'name': 'memory_ddr',
        'type': 'boolean',
        'default': False,
        'label': 'Memory - DDR (first generation)'
    },
    {
        'name': 'memory_ddr2',
        'type': 'boolean',
        'default': False,
        'label': 'Memory - DDR2'
    },
    {
        'name': 'memory_ddr3',
        'type': 'boolean',
        'default': False,
        'label': 'Memory - DDR3'
    },
    {
        'name': 'memory_ddr3l',
        'type': 'boolean',
        'default': False,
        'label': 'Memory - DDR3L'
    },
    {
        'name': 'memory_ddr4',
        'type': 'boolean',
        'default': False,
        'label': 'Memory - DDR4'
    },
    {
        'name': 'memory_ddr4lp',
        'type': 'boolean',
        'default': False,
        'label': 'Memory - DDR4LP'
    },
    {
        'name': 'memory_ddr5',
        'type': 'boolean',
        'default': False,
        'label': 'Memory - DDR5'
    },
    {
        'name': 'memory_ecc',
        'type': 'boolean',
        'default': False,
        'label': 'Memory - Error Correction Code (ECC) support'
    },
    {
        'name': 'memory_max',
        'type': 'integer',
        'unit': 'MiB',
        'default': 0,
        'label': 'Memory - maximum supported (or installed, if soldered)'
    },
    {
        'name': 'memory_slots',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Memory - slots'
    },
    {
        'name': 'memory_sodiimm',
        'type': 'boolean',
        'default': False,
        'label': 'Memory - SODIMM (Small Outline Dual Inline Memory Module)'
    },
    {
        'name': 'memory_speed',
        'type': 'integer',
        'unit': 'MHz',
        'default': 0,
        'label': 'Memory - speed'
    },
    {
        'name': 'mounting_holes',
        'type': 'boolean',
        'default': False,
        'label': 'Mounting holes'
    },
    {
        'name': 'pmic',
        'type': 'string',
        'default': '',
        'label': 'Power Management IC (PMIC)'
    },
    {
        'name': 'power_battery_connector',
        'type': 'boolean',
        'default': False,
        'label': 'Battery connector'
    },
    {
        'name': 'power_battery_type',
        'type': 'reference',
        'relation': 'TODO',
        'default': None,
        'label': 'Battery type'
    },
    {
        'name': 'power_input_current_max',
        'type': 'integer',
        'unit': 'mA',
        'default': 0,
        'label': 'Input current - maximum / peak consumption'
    },
    {
        'name': 'power_input_current_min',
        'type': 'integer',
        'unit': 'mA',
        'default': 0,
        'label': 'Input current - minimum / idle consumption'
    },
    {
        'name': 'power_input_voltage_max',
        'type': 'integer',
        'unit': 'mV',
        'default': 0,
        'label': 'Input voltage - maximum'
    },
    {
        'name': 'power_input_voltage_min',
        'type': 'integer',
        'unit': 'mV',
        'default': 0,
        'label': 'Input voltage - minimum'
    },
    {
        'name': 'rtc',
        'type': 'boolean',
        'default': False,
        'label': 'Real Time Clock (RTC) onboard'
    },
    {
        'name': 'sata_bootable',
        'type': 'boolean',
        'default': False,
        'label': 'SATA - bootable'
    },
    {
        'name': 'sata_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'SATA - interfaces'
    },
    {
        'name': 'sata_revision',
        'type': 'integer',
        'unit': 'revision',
        'default': 0,
        'label': 'SATA - revision (e.g. 3)'
    },
    {
        'name': 'sata_slots',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'SATA - slots'
    },
    {
        'name': 'sd_bootable',
        'type': 'boolean',
        'default': False,
        'label': 'SD(IO) - bootable'
    },
    {
        'name': 'sd_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'SD(IO) - interfaces'
    },
    {
        'name': 'sd_interface',
        'type': 'reference',
        'relation': 'storageinterface.yaml',
        'default': None,
        'label': 'SD(IO) - interface type'
    },
    {
        'name': 'sd_slots',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'SD(IO) - slots'
    },
    {
        'name': 'size_depth',
        'type': 'integer',
        'unit': 'mm',
        'default': 0,
        'label': 'Board dimensions - Depth'
    },
    {
        'name': 'size_width',
        'type': 'integer',
        'unit': 'mm',
        'default': 0,
        'label': 'Board dimensions - width'
    },
    {
        'name': 'size_height',
        'type': 'integer',
        'unit': 'mm',
        'default': 0,
        'label': 'Board dimensions - height'
    },
    {
        'name': 'software_android',
        'type': 'string',
        'default': '',
        'label': 'Software - Android support (version, if any)'
    },
    {
        'name': 'software_linux',
        'type': 'boolean',
        'default': False,
        'label': 'Software - Linux support'
    },
    {
        'name': 'software_linux_distros',
        'type': 'string',
        'default': '',
        'label': 'Software - Linux distributions supported (comma-separated list)'
    },
    {
        'name': 'software_linux_mainline',
        'type': 'boolean',
        'default': False,
        'label': 'Software - Linux mainline support'
    },
    {
        'name': 'software_other',
        'type': 'string',
        'default': '',
        'label': 'Software - Other operating systems supported (comma-separated list)'
    },
    {
        'name': 'software_windows',
        'type': 'string',
        'default': '',
        'label': 'Software - Windows support (version, if any)'
    },
    {
        'name': 'storage_emmc_bootable',
        'type': 'boolean',
        'default': False,
        'label': 'Storage - Embedded MultiMediaCard (eMMC) - bootable'
    },
    {
        'name': 'storage_emmc_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Storage - Embedded MultiMediaCard (eMMC)'
    },
    {
        'name': 'storage_emmc_interface',
        'type': 'reference',
        'relation': 'storageinterface.yaml',
        'default': None,
        'label': 'Storage - Embedded MultiMediaCard (eMMC) - interface type'
    },
    {
        'name': 'storage_emmc_size',
        'type': 'integer',
        'unit': 'MB',
        'default': 0,
        'label': 'Storage - Embedded MultiMediaCard (eMMC) - size'
    },
    {
        'name': 'storage_emmc_slotted',
        'type': 'boolean',
        'default': False,
        'label': 'Storage - Embedded MultiMediaCard (eMMC) - slotted'
    },
    {
        'name': 'storage_spiflash_bootable',
        'type': 'boolean',
        'default': False,
        'label': 'SPI flash - bootable'
    },
    {
        'name': 'storage_spiflash_size',
        'type': 'integer',
        'unit': 'MiB',
        'default': 0,
        'label': 'SPI flash - size'
    },
    {
        'name': 'temperature_max',
        'type': 'integer',
        'unit': 'Celsius degrees',
        'default': None,
        'label': 'Operating temperature - maximum (of range)'
    },
    {
        'name': 'temperature_min',
        'type': 'integer',
        'unit': 'Celsius degrees',
        'default': None,
        'label': 'Operating temperature - minimum (of range)'
    },
    {
        'name': 'usb_bootable',
        'type': 'boolean',
        'default': False,
        'label': 'USB - bootable'
    },
    {
        'name': 'usb_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'USB - interfaces (total)'
    },
    {
        'name': 'usb_device',
        'type': 'boolean',
        'default': False,
        'label': 'USB - device (client) interfaces'
    },
    {
        'name': 'usb_hosts',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'USB - host (server) supported interfaces'
    },
    {
        'name': 'usb_otg',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'USB - OTG-supported interfaces'
    },
    {
        'name': 'usb_slots',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'USB - slots'
    },
    {
        'name': 'usb_superspeed',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'USB - SuperSpeed (3.x / 4.x) supported interfaces'
    },
    {
        'name': 'usb_typec',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'USB - Type-C supported interfaces'
    },
    {
        'name': 'video_composite_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Composite (CVBS) video outputs'
    },
    {
        'name': 'video_composite_slots',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'TODO'
    },
    {
        'name': 'display_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Display outputs (all types) - TODO'
    },
    {
        'name': 'video_displayport_connector',
        'type': 'reference',
        'relation': 'displayconnector.yaml',
        'default': None,
        'label': 'Display outputs - connector'
    },
    {
        'name': 'video_displayport_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'DisplayPort outputs'
    },
    {
        'name': 'video_displayport_slots',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'DisplayPort slots'
    },
    {
        'name': 'video_displayport_standard',
        'type': 'reference',
        'relation': 'displayinterface.yaml',
        'default': None,
        'label': 'Display outputs - interface standard'
    },
    {
        'name': 'video_dpi_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Display outputs - parallel interface (DPI)'
    },
    {
        'name': 'video_dpi_standard',
        'type': 'reference',
        'relation': 'displayinterface.yaml',
        'default': None,
        'label': 'Display outputs - parallel interface (DPI) - standard'
    },
    {
        'name': 'video_dsi_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Display outputs - DSI'
    },
    {
        'name': 'video_dsi_standard',
        'type': 'reference',
        'relation': 'displayinterface.yaml',
        'default': None,
        'label': 'Display outputs - DSI - standard'
    },
    {
        'name': 'video_hdmi_connector',
        'type': 'reference',
        'relation': 'displayconnector.yaml',
        'default': None,
        'label': 'Display - HDMI - connector type'
    },
    {
        'name': 'video_hdmi_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Display - HDMI'
    },
    {
        'name': 'video_hdmi_slots',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Display - HDMI - slots'
    },
    {
        'name': 'video_hdmi_standard',
        'type': 'reference',
        'relation': 'displaystandard.yaml',
        'default': None,
        'label': 'Display - HDMI - standard'
    },
    {
        'name': 'video_lvds_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'Display - Low Voltage Differential Signaling (LVDS) - interfaces'
    },
    {
        'name': 'video_lvds_standard',
        'type': 'reference',
        'relation': 'displaystandard.yaml',
        'default': None,
        'label': 'Display - Low Voltage Differential Signaling (LVDS) - standard type'
    },
    {
        'name': 'video_svideo_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'S-Video interfaces'
    },
    {
        'name': 'video_svideo_slots',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'S-Video slots'
    },
    {
        'name': 'video_vga_connector',
        'type': 'reference',
        'relation': 'displayconnector.yaml',
        'default': None,
        'label': 'VGA - connector type'
    },
    {
        'name': 'video_vga_count',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'VGA - interfaces'
    },
    {
        'name': 'video_vga_slots',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'VGA - slots'
    },
    {
        'name': 'wireless_bluetooth_audio',
        'type': 'boolean',
        'default': False,
        'label': 'Bluetooth audio support'
    },
    {
        'name': 'wireless_bluetooth_ble',
        'type': 'boolean',
        'default': False,
        'label': 'Bluetooth BLE support'
    },
    {
        'name': 'wireless_bluetooth_onboard',
        'type': 'boolean',
        'default': False,
        'label': 'Onboard Bluetooth support'
    },
    {
        'name': 'wireless_bluetooth_version',
        'type': 'string',
        'default': None,
        'label': 'Bluetooth version (max, major.minor, e.g. 4.2)'
    },
    {
        'name': 'wireless_lora_onboard',
        'type': 'boolean',
        'default': False,
        'label': 'Wireless - Onboard LoRa (Long Range) modem'
    },
    {
        'name': 'wireless_mobile_modem_onboard',
        'type': 'boolean',
        'default': False,
        'label': 'Wireless - Onboard mobile broadband (cellular) modem'
    },
    {
        'name': 'wireless_wifi_2g_level',
        'type': 'integer',
        'unit': 'level',
        'default': 0,
        'label': 'WiFi - 2.4 GHz support. 0 = no support, 1 = b, 2 = g, 3 = n, 4 = ac'
    },
    {
        'name': 'wireless_wifi_5g_level',
        'type': 'integer',
        'unit': 'level',
        'default': 0,
        'label': 'WiFi - 5 GHz support. 0 = no support, 1 = a, 2 = n, 3 = ac, 4 = ax'
    },
    {
        'name': 'wireless_wifi_chains',
        'type': 'integer',
        'unit': 'count',
        'default': 0,
        'label': 'WiFi - number of chains (TODO)'
    },
    {
        'name': 'wireless_wifi_external_antenna',
        'type': 'boolean',
        'default': False,
        'label': 'WiFi - external antenna connector'
    },
    {
        'name': 'wireless_wifi_onboard',
        'type': 'boolean',
        'default': False,
        'label': 'Wireless - Onboard WiFi'
    }
]

def generate_yaml_template():
    global fields
    import yaml
    
    yaml_template = {}
    for field in fields:
        yaml_template[field['name']] = field['default']
    yaml_dump = yaml.dump(yaml_template)
    out = ''
    for line in yaml_dump.splitlines():
        line_field = line.split(':')[0]
        for field in fields:
            if field['name'] == line_field:
                out += '# ' + field['label']
                if 'unit' in field and field['unit'] != '':
                    out += ' (' + field['unit'] + ')'
                out += '\n'
        out += line + '\n'
    return out
